//
//  ColorConstants.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 02.05.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import Foundation
import UIKit


//MARK: - Views
public let mainColor = UIColor(red: 0x61/255, green: 0xD2/255, blue: 0xD6, alpha: 1)

struct NavigationBarColors {
    static let navigationBackground = UIColor(red: 55/255, green: 188/255, blue: 152/255, alpha: 1)
}
