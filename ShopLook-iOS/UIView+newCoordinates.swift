//
//  UIView+newCoordinates.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 07.05.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import Foundation
import UIKit
import CoreGraphics

extension UIView {
    /// Helper to get pre transform frame
    var originalFrame: CGRect {
        let currentTransform = transform
        transform = .identity
        let originalFrame = frame
        transform = currentTransform
        return originalFrame
    }
    
    /// Helper to get point offset from center
    func centerOffset(_ point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x - center.x, y: point.y - center.y)
    }
    
    /// Helper to get point back relative to center
    func pointRelativeToCenter(_ point: CGPoint) -> CGPoint {
        return CGPoint(x: point.x + center.x, y: point.y + center.y)
    }
    
    /// Helper to get point relative to transformed coords
    func newPointInView(_ point: CGPoint) -> CGPoint {
        // get offset from center
        let offset = centerOffset(point)
        // get transformed point
        let transformedPoint = offset.applying(transform)
        // make relative to center
        return pointRelativeToCenter(transformedPoint)
    }
    
    var newTopLeft: CGPoint {
        return newPointInView(originalFrame.origin)
    }
    
    var newTopRight: CGPoint {
        var point = originalFrame.origin
        point.x += originalFrame.width
        return newPointInView(point)
    }
    
    var newBottomLeft: CGPoint {
        var point = originalFrame.origin
        point.y += originalFrame.height
        return newPointInView(point)
    }
    
    var newBottomRight: CGPoint {
        var point = originalFrame.origin
        point.x += originalFrame.width
        point.y += originalFrame.height
        return newPointInView(point)
    }
    
    
    var ctp: CGPoint {
        var rect = self.bounds
        rect.origin.x = self.center.x - 0.5 * rect.width
        rect.origin.y = self.center.y - 0.5 * rect.height
        
        let originalTopLeftCorner = CGPoint(x:rect.minX, y:rect.minY)
        let rectCenter = CGPoint(x:rect.midX, y:rect.midY)
        let radius = sqrt(pow(rectCenter.x - originalTopLeftCorner.x, 2.0) + pow(rectCenter.y - originalTopLeftCorner.y, 2.0))
        
        let originalAngle = CGFloat.pi - acos((rectCenter.x - originalTopLeftCorner.x) / radius)
        let currentTransform = self.transform
        let rotation = CGFloat(atan2f(Float(CGFloat(currentTransform.b)), Float(currentTransform.a)))
        let resultAngle = originalAngle - rotation;
        let currentTopLeftCorner = CGPoint(x:round(rectCenter.x + cos(resultAngle) * radius), y:round(rectCenter.y - sin(resultAngle) * radius));
        return currentTopLeftCorner
    }
    
    func getCenterPoint(angle : CGFloat)-> CGPoint {
        var rect = self.bounds
        rect.origin.x = self.center.x - 0.5 * rect.width
        rect.origin.y = self.center.y - 0.5 * rect.height
        
        let originalTopLeftCorner = CGPoint(x:self.center.x, y:self.center.y)
        let rectCenter = CGPoint(x:rect.midX, y:rect.midY)
        let radius = sqrt(pow(rectCenter.x - originalTopLeftCorner.x, 2.0) + pow(rectCenter.y - originalTopLeftCorner.y, 2.0))
        
//        let originalAngle = CGFloat.pi - acos((rectCenter.x - originalTopLeftCorner.x) / radius)
//        let currentTransform = self.transform
//        let rotation = CGFloat(atan2f(Float(CGFloat(currentTransform.b)), Float(currentTransform.a)))
//        let resultAngle = originalAngle - rotation;
        let currentTopLeftCorner = CGPoint(x:round(rectCenter.x + cos(angle) * radius), y:round(rectCenter.y - sin(angle) * radius));
        return currentTopLeftCorner
        
        
    }
    
    
//    - (CGPoint)currentTopLeftPointOfTheView:(UIView *)view
//    {
//    CGRect rect = view.bounds;
//    rect.origin.x = view.center.x - 0.5 * CGRectGetWidth(rect);
//    rect.origin.y = view.center.y - 0.5 * CGRectGetHeight(rect);
//
//    CGPoint originalTopLeftCorner = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
//    CGPoint rectCenter = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
//    CGFloat radius = sqrt(pow(rectCenter.x - originalTopLeftCorner.x, 2.0) + pow(rectCenter.y - originalTopLeftCorner.y, 2.0));
//
//    CGFloat originalAngle = M_PI - acos((rectCenter.x - originalTopLeftCorner.x) / radius);
//
//    CATransform3D currentTransform = ((CALayer *)view.layer.presentationLayer).transform;
//    CGFloat rotation = atan2(currentTransform.m12, currentTransform.m11);
//
//    CGFloat resultAngle = originalAngle - rotation;
//    CGPoint currentTopLeftCorner = CGPointMake(round(rectCenter.x + cos(resultAngle) * radius), round(rectCenter.y - sin(resultAngle) * radius));
//
//    return currentTopLeftCorner;
//    }
}
