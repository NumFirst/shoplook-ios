//
//  clothItem.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 30.04.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import UIKit


protocol ClothItemDelegate {
    func clothItemBecameActive(_ clothItem: ClothItem)
}

class ClothItem: UIImageView, UIGestureRecognizerDelegate, NSCopying {
    var delegate: ClothItemDelegate?
    var isActive : Bool = false {
        didSet {
            self.layer.borderWidth = isActive ? 1 : 0
        }
    }
    var isHFlipped = false;
    var isVFlipped = false;
    var scale: CGFloat = 1;
    var rotation: CGFloat = 0;
    var left : CGFloat = 0;
    var top : CGFloat = 0;
    
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = ClothItem(clothImage: self.image)
        
        copy.scale      = scale
        copy.rotation   = rotation
        copy.isHFlipped = isHFlipped
        copy.isVFlipped = isVFlipped
        copy.isActive   = false
        
        copy.transform = copy.transform.scaledBy(x: self.scale, y: self.scale)
        copy.transform = copy.transform.rotated(by: copy.rotation)
        
        return copy
    }
    
    convenience init(serverModel : ClothItemServer) {
        if let image = serverModel.image {
           // self.image = image
            self.init(clothImage: image)
        } else {
            self.init()
        }
        
//
//
//        var t = self.transform
//        t = t.rotated(by: CGFloat.pi / 4)
//     //   t = t.translatedBy(x: 100, y: 300)
//
//       // t = t.scaledBy(x: -1, y: 2)
//        self.transform = t
//        t = t.translatedBy(x: 100, y: 300)
//        self.transform = t
//         print(self.frame)
    
        if let scale = serverModel.scaleX {
            self.scale = scale
           // self.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.transform = self.transform.scaledBy(x: self.scale, y: self.scale)
        }
        
       
       
        if let isHFlipped = serverModel.flipX {
           // self.isHFlipped = isHFlipped
            if isHFlipped {
                self.flipHorizontally()
            }
        }
        
        if let isVFlipped = serverModel.flipY {
           // self.isVFlipped = isVFlipped
            if isVFlipped {
                self.flipVertically()
            }
        }
      //  self.isActive   = true
        
        
        
        if let rotation = serverModel.angle {
            self.rotation   = rotation
      //      self.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.transform = self.transform.rotated(by: self.rotation)
        }
        
      
        
//        if let x = serverModel.left, let y = serverModel.top, let width = serverModel.width, let height = serverModel.height {
//
//            print("x:\(x), y:\(y), width:\(width), heigth\(height)")
//
//            self.frame.origin = CGPoint(x: 0, y: 0)
////            self.center = CGPoint(x: 300, y: 300)
//        }
        
     //     self.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        
        if let x = serverModel.left, let y = serverModel.top, let width = serverModel.width, let height = serverModel.height {
            
            //   print("x:\(x), y:\(y), width:\(width), heigth\(height)"
            self.left = x
            self.top = y
            //  self.layer.anchorPoint = CGPoint(x: 0, y: 0)
            print("Opened frame: \n x:\(x), \n y:\(y), \n width:\(width), \n heigth\(height)\n")
            
            
            print(self.frame)
            let xOffset = x - self.newTopLeft.x
            print("xOffset ::::", xOffset)
            let yOffset = y - self.newTopLeft.y
            print("yOffset ::::", yOffset)
      //      print(self.frame)
     //       self.frame = self.frame.offsetBy(dx: xOffset, dy: yOffset)
      //      print(self.frame)
            self.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
           // self.transform = self.transform.translatedBy(x: 300, y: 200)
            
            self.center = CGPoint(x:self.center.x + xOffset, y:self.center.y + yOffset)
            
            
//            originalTransform.translatedBy(x: (destinationX - x + width/2), y: destinationY - y + height/2)

            //   self.frame.origin = CGPoint(x: x, y: y)
          print(self.frame)
            //            self.frame.origin = CGPoint(x: 0, y: 0)
        }
 
    }
    
    
    convenience init(clothImage: UIImage?) {
        self.init()
        self.contentMode = .scaleAspectFit
        self.frame.size = CGSize(width: 200, height: 200)
        if let image = clothImage {
            self.image = image;
        }
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panPerformed(_:)))
        let pinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(pinchPerformed(_:)))
        let rotateGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(rotatePerformed(_:)))
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapPerformed(_:)))
        
        
        panGestureRecognizer.delegate = self;
        pinchGestureRecognizer.delegate = self;
        rotateGestureRecognizer.delegate = self;
        tapGestureRecognizer.delegate = self;
        
        self.layer.borderColor = mainColor.cgColor
        self.addGestureRecognizer(pinchGestureRecognizer)
        self.addGestureRecognizer(panGestureRecognizer)
        self.addGestureRecognizer(rotateGestureRecognizer)
        self.addGestureRecognizer(tapGestureRecognizer)
        
        self.isUserInteractionEnabled = true  
    }
    
//    func setupClothView {
//        
//        
//    }
//    
    
    //MARK : - Gesture Recognizers
    @objc func tapPerformed(_ sender: UITapGestureRecognizer) {
        if !isActive {
            delegate?.clothItemBecameActive(self)
            self.isActive = true;
        }
    }
    
    
    @objc func pinchPerformed(_ sender: UIPinchGestureRecognizer) {
        if let view = sender.view {
            if !isActive {
                delegate?.clothItemBecameActive(self)
                self.isActive = true;
            }
            
            view.transform = view.transform.scaledBy(x: sender.scale, y: sender.scale)
            
            if sender.state != .ended {
                scale = sender.scale * scale
            }
            //    view.transform = CGAffineTransform(scaleX: sender.scale, y: sender.scale)
            sender.scale = 1
        
            print("X :::: ", view.frame.origin.x)
            print("Y :::: ", view.frame.origin.y)
            
            print("X BOUNDS:::: ", view.newTopLeft.x)
            print("Y BOUNDS:::: ", view.newTopLeft.y)
            left = view.newTopLeft.x
            top = view.newTopLeft.y
        }
    }
    
    @objc func rotatePerformed(_ sender: UIRotationGestureRecognizer) {
        if let view = sender.view {
            if !isActive {
                delegate?.clothItemBecameActive(self)
                self.isActive = true;
            }
            view.transform = view.transform.rotated(by: sender.rotation)
            if sender.state != .ended {
                rotation = sender.rotation + rotation;
            }
            print(sender.rotation)
            //  print(rotation)
            // view.transform = CGAffineTransform(rotationAngle: rotation)
            sender.rotation = 0
            print("X :::: ", view.frame.origin.x)
            print("Y :::: ", view.frame.origin.y)
            print("X BOUNDS:::: ", view.newTopLeft.x)
            print("Y BOUNDS:::: ", view.newTopLeft.y)
            left = view.newTopLeft.x
            top = view.newTopLeft.y
        }
    }
    
    @objc func panPerformed(_ sender: UIPanGestureRecognizer) {
        let translation = sender.translation(in: superview)
        if let view = sender.view {
            if !isActive {
                delegate?.clothItemBecameActive(self)
                self.isActive = true;
            }
            view.center = CGPoint(x:view.center.x + translation.x,
                                  y:view.center.y + translation.y)
            print("X :::: ", view.frame.origin.x)
            print("Y :::: ", view.frame.origin.y)
            print("X BOUNDS:::: ", view.newTopLeft.x)
            print("Y BOUNDS:::: ", view.newTopLeft.y)
            left = view.newTopLeft.x
            top = view.newTopLeft.y
        }
        sender.setTranslation(CGPoint.zero, in: superview)
       
    }
    
    //MARK: - Image Utils
    func flipHorizontally() {
        isHFlipped = !isHFlipped
        self.image = ImageUtils().flipH(im: self.image!)
    }
    
    func flipVertically() {
        isVFlipped = !isVFlipped
        self.image = ImageUtils().flipV(im: self.image!)
    }
    
    //MARK : - Delegate Recognizer
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    
}
