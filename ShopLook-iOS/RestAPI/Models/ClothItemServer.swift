//
//  ClothItemServer.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 04.05.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import Foundation
import ObjectMapper

class ClothItemServer: NSObject, Mappable, NSCoding {
    
    
    
    var left: CGFloat?
    var top : CGFloat?
    var width : CGFloat?
    var height : CGFloat?
    var scaleX : CGFloat?
    var scaleY : CGFloat?
    var angle : CGFloat?
    var flipX : Bool?
    var flipY : Bool?
    var src : String?
    var itemID : String?
    //TODO: need remake for URL
    var image : UIImage?
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    convenience init(withClothItem: ClothItem) {
        self.init()
        scaleX = withClothItem.scale
        scaleY = withClothItem.scale
        angle  = withClothItem.rotation
        flipX  = withClothItem.isHFlipped
        flipY  = withClothItem.isVFlipped
        itemID = ""
        image = withClothItem.image
   
    
        //let tempItem = withClothItem.copy()
        
//        left   = withClothItem.frame.origin.x
//        top    = withClothItem.frame.origin.y
//        width  = withClothItem.frame.width
//        height = withClothItem.frame.height

        left   = withClothItem.left
        top    = withClothItem.top
        width  = withClothItem.frame.width
        height = withClothItem.frame.height
        
        print("Saving frame: \n x:\(left!), \n y:\(top!), \n width:\(width!), \n heigth\(height!) \n")
    }
    
    func mapping(map: Map) {
        left   <- map["left"]
        top    <- map["top"]
        width  <- map["width"]
        height <- map["height"]
        scaleX <- map["scaleX"]
        scaleY <- map["scaleY"]
        angle  <- map["angle"]
        flipX  <- map["flipX"]
        flipY  <- map["flipY"]
        itemID <- map["itemId"]
    }
    
    required init(coder aDecoder: NSCoder) {
        self.image = UIImage(data: aDecoder.decodeObject(forKey: "image")  as! Data)
       // print(image?.size.height, image?.size.width)
        self.left   = CGFloat(aDecoder.decodeFloat(forKey: "left"))
        self.top    = CGFloat(aDecoder.decodeFloat(forKey: "top"))
        self.width  = CGFloat(aDecoder.decodeFloat(forKey: "width"))
        self.height = CGFloat(aDecoder.decodeFloat(forKey: "height"))
        self.scaleX = CGFloat(aDecoder.decodeFloat(forKey: "scaleX"))
        self.scaleY = CGFloat(aDecoder.decodeFloat(forKey: "scaleY"))
        self.angle  = CGFloat(aDecoder.decodeFloat(forKey: "angle"))
     
        self.flipY  = aDecoder.decodeBool(forKey: "flipY")
        self.flipX  = aDecoder.decodeBool(forKey: "flipX")
        self.itemID = aDecoder.decodeObject(forKey: "itemID") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        
      //  print(image?.size.height, image?.size.width)
      
        if let image = image {
            aCoder.encode(UIImagePNGRepresentation(image), forKey: "image")
        }
        
        if let left = left {
            aCoder.encode(Float(left), forKey: "left")
            print("left: \(left)")
        }
        if let top = top {
            aCoder.encode(Float(top), forKey: "top")
            print("top: \(top)")
        }
        
        if let width = width {
            aCoder.encode(Float(width), forKey: "width")
            print("width: \(width)")
        }
        
        if let height = height {
            aCoder.encode(Float(height), forKey: "height")
            print("height: \(height)")
        }
        
        
        if let scaleX = scaleX {
            aCoder.encode(Float(scaleX), forKey: "scaleX")
          //   print("scaleX: \(scaleX)")
        }
        
        if let scaleY = scaleY {
            aCoder.encode(Float(scaleY), forKey: "scaleY")
        }
        
        if let angle = angle {
            aCoder.encode(Float(angle), forKey: "angle")
        }
        
        if let flipY = flipY {
            aCoder.encode(flipY, forKey: "flipY")
        }
        
        if let flipX = flipX {
            aCoder.encode(flipX, forKey: "flipX")
        }
        
        if let itemID = itemID {
            aCoder.encode(itemID, forKey: "itemID")
        }
    }
   
        
     
    
}
//"save_data":"{
//\"version\":\"2.2.3\",
//\"objects\":[
//{
//    \"type\":\"image\",
//    \"version\":\"2.2.3\",
//    \"originX\":\"left\",
//    \"originY\":\"top\",
//    \"left\":261,
//    \"top\":314,
//    \"width\":500,
//    \"height\":500,
//    \"fill\":\"rgb(0,
//    0,
//    0         )\",
//    \"stroke\":null,
//    \"strokeWidth\":0,
//    \"strokeDashArray\":null,
//    \"strokeLineCap\":\"butt\",
//    \"strokeLineJoin\":\"miter\",
//    \"strokeMiterLimit\":10,
//    \"scaleX\":0.93,
//    \"scaleY\":0.93,
//    \"angle\":0,
//    \"flipX\":false,
//    \"flipY\":false,
//    \"opacity\":1,
//    \"shadow\":null,
//    \"visible\":true,
//    \"clipTo\":null,
//    \"backgroundColor\":\"\",
//    \"fillRule\":\"nonzero\",
//    \"paintFirst\":\"fill\",
//    \"globalCompositeOperation\":\"source-over\",
//    \"transformMatrix\":null,
//    \"skewX\":0,
//    \"skewY\":0,
//    \"crossOrigin\":\"\",
//    \"cropX\":0,
//    \"cropY\":0,
//    \"src\":\"/s3img/products/24203-1-ab64c.png\",
//    \"filters\":[
//    {
//        \"type\":\"RemoveColor\",
//        \"color\":\"#FFFFFF\",
//        \"distance\":0.1
//    }
//    ],
//    \"itemId\":24203
//},
//{
//    \"type\":\"image\",
//    \"version\":\"2.2.3\",
//    \"originX\":\"left\",
//    \"originY\":\"top\",
//    \"left\":295,
//    \"top\":327,
//    \"width\":400,
//    \"height\":400,
//    \"fill\":\"rgb(0,
//    0,
//    0         )\",
//    \"stroke\":null,
//    \"strokeWidth\":0,
//    \"strokeDashArray\":null,
//    \"strokeLineCap\":\"butt\",
//    \"strokeLineJoin\":\"miter\",
//    \"strokeMiterLimit\":10,
//    \"scaleX\":1,
//    \"scaleY\":1,
//    \"angle\":0,
//    \"flipX\":false,
//    \"flipY\":false,
//    \"opacity\":1,
//    \"shadow\":null,
//    \"visible\":true,
//    \"clipTo\":null,
//    \"backgroundColor\":\"\",
//    \"fillRule\":\"nonzero\",
//    \"paintFirst\":\"fill\",
//    \"globalCompositeOperation\":\"source-over\",
//    \"transformMatrix\":null,
//    \"skewX\":0,
//    \"skewY\":0,
//    \"crossOrigin\":\"\",
//    \"cropX\":0,
//    \"cropY\":0,
//    \"src\":\"/s3img/products/20316-1-a5078.png\",
//    \"filters\":[
//    {
//        \"type\":\"RemoveColor\",
//        \"color\":\"#FFFFFF\",
//        \"distance\":0.32
//    }
//    ],
//    \"itemId\":20316
//}
//],
//\"width\":1015,
//\"height\":1114
//}
