//
//  Settings.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 04.05.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import Foundation

#if DEVELOPMENT
let SERVER_URL = "https://shoplookio-api-staging.herokuapp.com/api/outfits/"
let VERSION_CHECK = "DEVELOPMENT VERSION"
#else
let SERVER_URL = "www"
let VERSION_CHECK = "PRODUCTION VERSION"
#endif
