//
//  ViewController.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 30.04.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import UIKit

class CanvasViewController: UIViewController,  ClothItemDelegate {

    
    @IBOutlet weak var controlPanel: UIView!
    @IBOutlet weak var canvasView: UIView!
    var clothItems :[ClothItem] = []
    var savedItems :[ClothItem] = []
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func plusDidTap(_ sender: Any) {
        
        let itemView = ClothItem(clothImage: UIImage(named: "coat")!)
        itemView.center = self.view.center
        itemView.delegate = self;
        canvasView.addSubview(itemView)
        clothItems.append(itemView)
        
    }

   
    @IBAction func clearButtonDidTap(_ sender: Any) {
        clearCanvas()
    }
    
    func activeClothItem() -> ClothItem? {
        let activeItems = clothItems.filter({$0.isActive})
        if activeItems.count > 0 {
            return activeItems[0]
        }
       return nil
    }
    
    func indexAsSubviewOfClothItem() -> Int? {
        guard let activeItem = activeClothItem() else {
            return nil
        }
        return  canvasView.subviews.index(of: activeItem)
    }
    
    func clothItemBecameActive(_ clothItem: ClothItem) {
        for item in clothItems {
            if item != clothItem && item.isActive {
                item.isActive = false;
            }
        }
    }
    
    @IBAction func toFrontButtonDidPress(_ sender: Any) {
        guard let indexActiveItem = indexAsSubviewOfClothItem() else {
            return
        }
        if  indexActiveItem < clothItems.count - 1 {
            self.canvasView.exchangeSubview(at: indexActiveItem, withSubviewAt: indexActiveItem + 1)
        }
    }
    
    @IBAction func toBackButtonDidPress(_ sender: Any) {
        guard let indexActiveItem = indexAsSubviewOfClothItem() else {
            return
        }
        if  indexActiveItem > 0 {
            self.canvasView.exchangeSubview(at: indexActiveItem, withSubviewAt: indexActiveItem - 1)
        }
    }
    
    @IBAction func removeButtonDidPress(_ sender: Any) {
        activeClothItem()?.removeFromSuperview()
    }
    
    @IBAction func flipImageHorizontButtonDidPress(_ sender: Any) {
        guard let activeItem = activeClothItem() else {
            return
        }
        activeItem.flipHorizontally()
            //.transform = CGAffineTransform(scaleX: -1, y: 1);
//        if activeItem.image?.imageOrientation == .leftMirrored {
//            activeItem.image = UIImage(cgImage: (activeItem.image?.cgImage)!, scale: 1.0, orientation: .rightMirrored)
//        } else {
//            activeItem.image = UIImage(cgImage: (activeItem.image?.cgImage)!, scale: 1.0, orientation: .leftMirrored)
//        }
//
        
    }
    
    @IBAction func flipImageVerticalButtonDidPress(_ sender: Any) {
        
        guard let activeItem = activeClothItem() else {
            return
        }
        activeItem.flipVertically()
//
//        if activeItem.image?.imageOrientation == .downMirrored {
//            activeItem.image = UIImage(cgImage: (activeItem.image?.cgImage)!, scale: 1.0, orientation: .upMirrored)
//        } else {
//            activeItem.image = UIImage(cgImage: (activeItem.image?.cgImage)!, scale: 1.0, orientation: .downMirrored)
//        }
        
        
    }
    @IBAction func duplicateButtonDidPress(_ sender: Any) {
        guard let activeItem = activeClothItem() else {
            return
        }
        let item = activeItem.copy() as! ClothItem
        item.center = self.view.center
        item.delegate = self
        canvasView.addSubview(item)
        clothItems.append(item)
    }
    
    @IBAction func saveDraftButtonDidPress(_ sender: Any) {
        savedItems = clothItems
        var serverItems : [ClothItemServer] = []
        savedItems.forEach({
       //     canvasView.addSubview($0)
         //   print("frame: \($0.frame)")
            let item = ClothItemServer(withClothItem: $0)
            serverItems.append(item)
        })
        
        let savedItemsData = NSKeyedArchiver.archivedData(withRootObject: serverItems)
        UserDefaults.standard.set(savedItemsData, forKey: "savedItems")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func openDraftButtonDidPress(_ sender: Any) {
        clearCanvas()
        loadDraft()
    }
    
    
    func clearCanvas() {
        canvasView.subviews.forEach({ $0.removeFromSuperview() })
        clothItems = []
    }
    
    func loadDraft() {
        
        let savedItemsData = UserDefaults.standard.object(forKey: "savedItems") as? Data

        if let savedItemsData = savedItemsData {
            let itemsArray = NSKeyedUnarchiver.unarchiveObject(with: savedItemsData) as? [ClothItemServer]
            if let itemsArray = itemsArray {
                savedItems = []
                itemsArray.forEach({
                    let item = ClothItem(serverModel: $0)
                    savedItems.append(item)
                    item.delegate = self;
                })
                
                savedItems.forEach({canvasView.addSubview($0)
                    print("Added frame: \n x:\($0.newTopLeft.x), \n y:\($0.newTopLeft.y), \n width:\($0.frame.width), \n heigth\($0.frame.height)\n")
                    
                    print("CPT ::::: \($0.ctp)")
           //         clothItems.append($0)
                })
                clothItems = savedItems
                
            }

        }
      
//        savedItems.forEach({canvasView.addSubview($0)})
//        clothItems = savedItems
       
    }
    
}

