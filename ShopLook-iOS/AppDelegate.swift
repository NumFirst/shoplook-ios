//
//  AppDelegate.swift
//  ShopLook-iOS
//
//  Created by Alex Suvorov on 30.04.2018.
//  Copyright © 2018 kinect.pro. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let version : Any! = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")
        let build : Any! = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")
        print(VERSION_CHECK,"v", version, "Build",build)
        return true
    }

}

